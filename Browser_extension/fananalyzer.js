function analyzer() {

var body = document.documentElement.innerHTML;
var lc = body.toLowerCase();
var article_text = lc.split(' ');

var team_list     = ['addict', 'adherent', 'admirer', 'advocate', 'aficionado', 'ally',
                     'apologist', 'backer', 'believer', 'benefactor', 'buff', 'champion',
                     'community', 'comrade', 'contributor', 'crowd', 'defender', 'devotee',
                     'disciple', 'donor', 'enthusiast', 'expert', 'fan', 'fiend', 'follower',
                     'freak', 'groupie', 'guest', 'helper', 'home', 'insider', 'junkie',
                     'local', 'lover', 'maniac', 'member', 'partisan', 'proponent', 'sponsor',
                     'subscriber', 'supporter', 'together', 'us', 'votary', 'voter', 'we'];
    
var emotion_list  = ['acclaim', 'accolade', 'admire', 'admire', 'applaud', 'appreciate', 'bravo',
                     'cheer', 'clap', 'comeback', 'congratulate', 'encourage', 'extol', 'fire',
                     'fun', 'glorify', 'hail', 'holler', 'honor', 'hoop', 'huzzah', 'improve',
                     'inspire', 'laud', 'laud', 'laurel', 'motivate', 'ovation', 'plaudit',
                     'praise', 'praise', 'rally', 'react', 'rebound', 'resurge', 'revive', 'roar',
                     'root', 'salute', 'scream', 'screech', 'shout', 'shriek', 'toast', 'welcome',
                     'whistle', 'yell'];
    
var activity_list = ['arena', 'assembly', 'audience', 'break', 'camp', 'cavalcade', 'celebration',
                     'ceremony', 'chapter', 'colloquium', 'con', 'concert', 'conclave',
                     'conference', 'convention', 'council', 'demo', 'demonstration', 'diet',
                     'display', 'event', 'fair', 'festival', 'forum', 'gathering', 'holiday',
                     'march', 'march', 'meeting', 'pageant', 'parade', 'park', 'platform',
                     'procession', 'respite', 'sojourn', 'spectacle', 'summit', 'symposium',
                     'synod', 'tour', 'trip', 'vacation'];
    
var action_list   = ['acquire', 'appear', 'arrange', 'attend', 'award', 'begin', 'behold', 'buy',
                     'celebrate', 'coordinate', 'create', 'examine', 'explore', 'eye', 'feast',
                     'found', 'frequent', 'gaze', 'glimpse', 'go', 'handle', 'hang', 'haunt',
                     'innovate', 'inspect', 'manage', 'mobilize', 'observe', 'orchestrate',
                     'organize', 'originate', 'patronize', 'plan', 'present', 'purchase', 'ready',
                     'regard', 'reserve', 'run', 'scan', 'schedule', 'scrutinize', 'show', 'study',
                     'survey', 'tackle', 'view', 'visit', 'watch', 'witness'];
    
var subcult_list  = ['alienate', 'alternative', 'amateur', 'bizarre', 'counterculture', 'cult',
                     'curate', 'eccentric', 'exotic', 'fresh', 'iconoclast', 'independent',
                     'indie', 'insider', 'irregular', 'mainstream', 'new', 'nonconformist',
                     'novel', 'offbeat', 'open', 'ostentatious', 'outlandish', 'outsider',
                     'provocative', 'radical', 'rebel', 'revolutionary', 'scary', 'scene',
                     'secret', 'social', 'subversive', 'tribe', 'uncommon', 'underground',
                     'unusual', 'urban', 'vibe'];

var team_count     = 0;
var emotion_count  = 0;
var activity_count = 0;
var action_count   = 0;
var subcult_count  = 0;
var keywords       = 0;

var ar_length = article_text.length();
for (var i = 0; i < ar_length; i++) {
    article_text[i] = stemmer(article_text[i]);
}

var l_length = team_list.length();
for (var i = 0; i < l_length; i++) {
    team_list[i] = stemmer(team_list[i]);
}

var l_length = emotion_list.length();
for (var i = 0; i < l_length; i++) {
    emotion_list[i] = stemmer(emotion_list[i]);
}

var l_length = activity_list.length();
for (var i = 0; i < l_length; i++) {
    activity_list[i] = stemmer(activity_list[i]);
}

var l_length = action_list.length();
for (var i = 0; i < l_length; i++) {
    action_list[i] = stemmer(action_list[i]);
}

var l_length = subcult_list.length();
for (var i = 0; i < l_length; i++) {
    subcult_list[i] = stemmer(subcult_list[i]);
}

for (var i = 0; i < ar_length; i++) {
    if (article_text[i] in team_list) {
    team_count += 1; 
    }
    else if (article_text[i] in emotion_list) {
    emotion_count += 1;
    }
    else if (article_text[i] in activity_list) {
    activity_count += 1;
    }
    else if (article_text[i] in action_list) {
    action_count += 1;
    }
    else if (article_text[i] in subcult_list) {
    subcult_count += 1;
    }
}

keywords = team_count + emotion_count + activity_count + action_count + subcult_count;

var text = '';
if (keywords === 0) {
        text = 'No results to analyze';
    }
    
else {
        text = 'Team orientation score was: ' + team_count +
        ' Emotional impact score was: ' + emotion_count + ' Activity orientation score was: '
        + activity_count + ' Action orientation score was: ' + action_count +
        ' Subculture orientation score was: ' + subc_count;	
    }
return text;

}
window.onload = analyzer;

