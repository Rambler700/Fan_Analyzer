# -*- coding: utf-8 -*-
"""
Created on Thu May 17 14:06:42 2018

@author: enovi
"""

import nltk

#Fan Analyzer

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Fan Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             segment_length = get_length()
             analyze_text(article_text,segment_length)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def get_length():
    c = input('Split article into segments y/n? ')
    if c == 'y':
        segment_length = int(input('Enter the number of segments to split the text into: '))
    else:
        segment_length = 1
    if segment_length < 1:
        segment_length = 1
    return segment_length

def analyze_text(article_text,segment_length):
    #team orientation, emotional impact, activity orientation, action orientation, subculture orientation
    team_list     = ['addict', 'adherent', 'admirer', 'advocate', 'aficionado', 'ally',
                     'apologist', 'backer', 'believer', 'benefactor', 'buff', 'champion',
                     'community', 'comrade', 'contributor', 'crowd', 'defender', 'devotee',
                     'disciple', 'donor', 'enthusiast', 'expert', 'fan', 'fiend', 'follower',
                     'freak', 'groupie', 'guest', 'helper', 'home', 'insider', 'junkie',
                     'local', 'lover', 'maniac', 'member', 'partisan', 'proponent', 'sponsor',
                     'subscriber', 'supporter', 'together', 'us', 'votary', 'voter', 'we']
    
    emotion_list  = ['acclaim', 'accolade', 'admire', 'admire', 'applaud', 'appreciate', 'bravo',
                     'cheer', 'clap', 'comeback', 'congratulate', 'encourage', 'extol', 'fire',
                     'fun', 'glorify', 'hail', 'holler', 'honor', 'hoop', 'huzzah', 'improve',
                     'inspire', 'laud', 'laud', 'laurel', 'motivate', 'ovation', 'plaudit',
                     'praise', 'praise', 'rally', 'react', 'rebound', 'resurge', 'revive', 'roar',
                     'root', 'salute', 'scream', 'screech', 'shout', 'shriek', 'toast', 'welcome',
                     'whistle', 'yell']
    
    activity_list = ['arena', 'assembly', 'audience', 'break', 'camp', 'cavalcade', 'celebration',
                     'ceremony', 'chapter', 'colloquium', 'con', 'concert', 'conclave',
                     'conference', 'convention', 'council', 'demo', 'demonstration', 'diet',
                     'display', 'event', 'fair', 'festival', 'forum', 'gathering', 'holiday',
                     'march', 'march', 'meeting', 'pageant', 'parade', 'park', 'platform',
                     'procession', 'respite', 'sojourn', 'spectacle', 'summit', 'symposium',
                     'synod', 'tour', 'trip', 'vacation']
    
    action_list   = ['acquire', 'appear', 'arrange', 'attend', 'award', 'begin', 'behold', 'buy',
                     'celebrate', 'coordinate', 'create', 'examine', 'explore', 'eye', 'feast',
                     'found', 'frequent', 'gaze', 'glimpse', 'go', 'handle', 'hang', 'haunt',
                     'innovate', 'inspect', 'manage', 'mobilize', 'observe', 'orchestrate',
                     'organize', 'originate', 'patronize', 'plan', 'present', 'purchase', 'ready',
                     'regard', 'reserve', 'run', 'scan', 'schedule', 'scrutinize', 'show', 'study',
                     'survey', 'tackle', 'view', 'visit', 'watch', 'witness']
    
    subcult_list  = ['alienate', 'alternative', 'amateur', 'bizarre', 'counterculture', 'cult',
                     'curate', 'eccentric', 'exotic', 'fresh', 'iconoclast', 'independent',
                     'indie', 'insider', 'irregular', 'mainstream', 'new', 'nonconformist',
                     'novel', 'offbeat', 'open', 'ostentatious', 'outlandish', 'outsider',
                     'provocative', 'radical', 'rebel', 'revolutionary', 'scary', 'scene',
                     'secret', 'social', 'subversive', 'tribe', 'uncommon', 'underground',
                     'unusual', 'urban', 'vibe']
    
    team_count     = 0
    emotion_count  = 0
    activity_count = 0
    action_count   = 0
    subcult_count  = 0
    keywords       = 0
    
    #split text string into individual words for analysis
    article_text = article_text.lower()
    article_text = article_text.split(' ')
    
    #stemming
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
    
    for word in team_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in emotion_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in activity_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in action_list:
        word = nltk.PorterStemmer().stem(word)    
        
    for word in subcult_list:
        word = nltk.PorterStemmer().stem(word)    
            
    section = int(len(article_text) / segment_length)    
    
    for i in range(1,segment_length + 1):
        selected_text = article_text[section * (i-1):section * i]
        
        for word in selected_text:
            if word in team_list:
                team_count     += 1
            elif word in emotion_list:
                emotion_count  += 1
            elif word in activity_list:
                activity_count += 1
            elif word in action_list:
                action_count   += 1
            elif word in subcult_list:
                subcult_count  += 1
                
        keywords = team_count + emotion_count + activity_count + action_count + subcult_count 

        print('Segment {} of {}.'.format(i,segment_length))                
        if keywords > 0:
            print('Team oriention score was:',(team_count/len(selected_text)))
            print('Emotional impact score was:',(emotion_count/len(selected_text)))
            print('Activity orientation score was:',(activity_count/len(selected_text)))
            print('Action orientation score was:',(action_count/len(selected_text)))
            print('Subculture orientation score was:',(subcult_count/len(selected_text)))
        else:
            print('No keywords appeared in the text.')
        #reset counters for next segment
        team_count     = 0
        emotion_count  = 0
        activity_count = 0
        action_count   = 0
        subcult_count  = 0    
        
main()